/*
 * pf_2_xfrm.h
 *
 *  Created on: Jul 8, 2015
 *      Author: sharani
 */

#ifndef PF_2_XFRM_H_
#define PF_2_XFRM_H_

extern int pf_key_2_xfrm(struct sa *sa, struct sa *isakmp_sa);

#endif /* PF_2_XFRM_H_ */
