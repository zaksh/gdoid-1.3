/*
 * pf_2_xfrm.c
 *
 *  Created on: Jul 8, 2015
 *      Author: sharani
 */

#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "ipsec.h"
#include "conf.h"
#include "sa.h"
#include "log.h"

int
pf_key_2_xfrm(
struct sa *sa, struct sa *isakmp_sa)
//klips_2_xfrm(const struct encap_msghdr const *emsg,
//		const struct proto const *proto,
//		u_int64_t seconds)
{
	int ret=-1;
	struct proto *proto = TAILQ_FIRST (&sa->protos);
	struct ipsec_sa *isa = sa->data;
	  char *s_addr=malloc(16*sizeof(char));
	  char *d_addr=malloc(16*sizeof(char));
	  struct ipsec_proto *iproto = proto->data;
	  u_int8_t *enc_key=NULL;
	  u_int8_t *auth_key=NULL;
	    size_t enc_key_sz;
	    size_t auth_key_sz;
	   char *spi=malloc(*proto->spi_sz*sizeof(char));
	   sprintf(spi,"%#x",decode_32(proto->spi[0]));
	   int limitSize=25;//temp size, need to fix it
	   char *limitTime=malloc(limitSize*sizeof(char));
	   sprintf(limitTime,"%d",sa->seconds);

	    if(get_keys(&enc_key,&auth_key,&enc_key_sz,&auth_key_sz,proto))
	    {
	    	log_error("klips_enable_sa::GetKeys: Unable to get keys");
	    	return -1;
	    }

	    char *e_key_hex=malloc((enc_key_sz*2 + 3)*sizeof(char));
	    char *a_key_hex=malloc((auth_key_sz*2 + 3)*sizeof(char));
	    strcpy(e_key_hex,"0x");
	    strcpy(a_key_hex,"0x");

	    int i;
		for(i=0;i<enc_key_sz;i++) {
			sprintf(e_key_hex+((i+1)*2), "%02x", enc_key[i]);
		}
		for(i=0;i<auth_key_sz;i++) {
			sprintf(a_key_hex+((i+1)*2), "%02x", auth_key[i]);
		}

		struct in_addr source;
		source.s_addr=isa->src_net;
		struct in_addr dst;
		dst.s_addr=isa->dst_net;
		strncpy(d_addr,inet_ntoa(dst),16);
		strncpy(s_addr,inet_ntoa(source),16);


	  LOG_DBG ((LOG_SYSDEP, 50,
	  	    	   	   		    "klips_2_xfrm: SPI-s: %s",spi));
		LOG_DBG ((LOG_SYSDEP, 50,
					"klips_2_xfrm: Limit-Time= %s",limitTime));
		LOG_DBG ((LOG_SYSDEP, 50,
				"klips_2_xfrm: enc_key= %s ,enc_key_sz=%d",
				enc_key,enc_key_sz));
		LOG_DBG ((LOG_SYSDEP, 50,
				"klips_2_xfrm: auth_key= %s ,auth_key_sz=%d"
		,auth_key,auth_key_sz));
	  LOG_DBG ((LOG_SYSDEP, 50,
	  	   		    "klips_2_xfrm: klips_enable_sa -1- Src:%s Dst:%s",s_addr,d_addr));


	  ret=set_xfrm_state(s_addr,d_addr,spi,limitTime,a_key_hex,e_key_hex);
	  if(ret)
	  {
		  goto cleanup;
	  }
	  ret=set_xfrm_policy(s_addr,d_addr,limitTime,is_this_source(s_addr));


cleanup:
	  	free(enc_key);
		free(auth_key);
		free(s_addr);
		free(d_addr);
	  	free(spi);
	  	free(e_key_hex);
	  	free(a_key_hex);
	  	free(limitTime);
		return ret;
}
int
set_xfrm_state(char *s_addr,char *d_addr,char *spi,char *limitTime,char *a_key_hex,char *e_key_hex)
{
		char message_xfrm[1024];//={'\0'};
		strcpy(message_xfrm,"ip xfrm state add src ");
		strcat(message_xfrm,s_addr);
		strcat(message_xfrm," dst ");
		strcat(message_xfrm,d_addr);
		strcat(message_xfrm," proto esp spi ");
		strcat(message_xfrm,spi);
		strcat(message_xfrm," mode tunnel reqid 0x00000001 auth sha1 ");
		strcat(message_xfrm,a_key_hex);
		strcat(message_xfrm," enc des3_ede ");
		strcat(message_xfrm,e_key_hex);
		strcat(message_xfrm," limit time-hard ");
		strcat(message_xfrm,limitTime);
		return WEXITSTATUS(system(message_xfrm)); //pass_message_to_xfrm(message_xfrm);
}
int
set_xfrm_policy(char *s_addr,char *d_addr, char *limitTime,int is_dir_out)
{
		char message_xfrm[1024]={'\0'};
		strcpy(message_xfrm,"ip xfrm policy add src ");
		strcat(message_xfrm,s_addr);
		strcat(message_xfrm," dst ");
		strcat(message_xfrm,d_addr);
		strcat(message_xfrm," dir ");
		strcat(message_xfrm,is_dir_out?"out":"in");
		strcat(message_xfrm," tmpl src ");
		strcat(message_xfrm,s_addr);
		strcat(message_xfrm," dst ");
		strcat(message_xfrm,d_addr);
		strcat(message_xfrm," proto esp reqid 0x00000001 mode tunnel");
		strcat(message_xfrm," limit time-hard ");
	    strcat(message_xfrm,limitTime);
		return WEXITSTATUS(system(message_xfrm)); //pass_message_to_xfrm(message_xfrm);
}
int
is_this_source(char *s_addr)
{
	struct conf_list *listen_on;
	struct conf_list_node *address;
	struct in_addr addr;
	in_addr_t if_addr=inet_addr(s_addr) ;
	listen_on = conf_get_list ("General", "Listen-on");
	  if (listen_on)
	    {
	      for (address = TAILQ_FIRST (&listen_on->fields); address;
		   address = TAILQ_NEXT (address, link))
		{
		  if (!inet_aton (address->field, &addr))
		    {
		      log_print ("is_source: invalid address %s in \"Listen-on\"",
				 address->field);
		      continue;
		    }

		  /* If found, take the easy way out.  */
		  if (addr.s_addr == if_addr)
		    break;
		}
	      conf_free_list (listen_on);

	      /*
	       * If address is zero then we did not find the address among the ones
	       * we should listen to.
	       * XXX We do not discover if we do not find our listen addresses...
	       * Maybe this should be the other way round.
	       */
	      if (address)
		return 1;
	    }
	  return 0;
}
/*
int
pass_message_to_xfrm(char *input)
{
	if(input==NULL)
	{
		log_error("Klips.c: PassMessageToXfrm empty input");
		return -1;
	}
	char argc=1;
	int i=0;
	printf("input= ");
	while(input[i]!='\0')
	{
		if(input[i]==' ')
		{
			argc++;
		}
		printf("%c",input[i]);
		i++;
	}
	printf("\n");
	char **argv;
	LOG_DBG ((LOG_SYSDEP, 90,
		 		  	   		    "pass_message_to_xfrm: input:%s",input));
	 LOG_DBG ((LOG_SYSDEP, 90,
		  	   		    "pass_message_to_xfrm: argc:%d",argc));

	argv=malloc(argc*sizeof(char*));
	i=0;
	char *token = strtok(input, " ");
	while(token) {
		argv[i++]=strdup(token);
		token = strtok(NULL, " ");
	}
	int ret=mymain(argc,argv);
	if(argv!=NULL)
	{
		for(i=0; i<argc; i++)
		{
			if(argv[i])
			{
				free(argv[i]);
			}
		}
		free(argv);
	}
	return ret;
}
*/
int
get_keys(u_int8_t **enc_key,u_int8_t **auth_key,
		size_t *enc_key_sz,size_t *auth_key_sz,
		const struct proto const *proto
		)
{
	  struct ipsec_proto *iproto = proto->data;
	  *enc_key_sz = ipsec_esp_enckeylength(proto);
	  *auth_key_sz = ipsec_esp_authkeylength(proto);
	//  u_int8_t * keymat=iproto->keymat[0];
	    if (*enc_key_sz)
	    	{
	    	  *enc_key = malloc(*enc_key_sz);
	    	  if (!*enc_key)
	    		{
	  	  	  return -1;
	  		}
	      //memcpy(*enc_key, keymat, *enc_key_sz);
	      memcpy(*enc_key, iproto->keymat[0], *enc_key_sz);
	  	}

	    if (*auth_key_sz)
	    	{
	    	  *auth_key = malloc(*auth_key_sz);
	    	  if (!*auth_key)
	    		{
	  	  	  return -1;
	  		}
	    	  //memcpy(*auth_key, (keymat+*enc_key_sz),
	    	  memcpy(*auth_key, (iproto->keymat[0]+*enc_key_sz),
	  	  		 *auth_key_sz);
	  	}
	    return 0;
}
