# README #

This is GDOID 1.3 code which works with NETKEY/XFRM sockets for Multicast IPSec.

### Changes made in original source code ###

* Changes made in original source code are listed in file [gdoi-1.3-changes](https://bitbucket.org/zaksh/gdoid-1.3/src/8e63e2b4efef568110963ee71e1f03d127404c0f/gdoi-1.3-changes?at=master)